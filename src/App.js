import React from 'react';
import './App.css';
import HeadSection from './components/HeadSection/HeadSection';
import FooterSection from './components/FooterSection/FooterSection';
import AboutSection from './components/AboutSection/AboutSection';
import ProductsSection from './components/ProductsSection/ProductsSection';
import ConnectSection from './components/ConnectSection/ConnectSection';

function App() {
  return (
    <div className="App">
      <HeadSection />
      <AboutSection />
      <ProductsSection />
      <ConnectSection />
      <FooterSection />
    </div>
  );
}

export default App;
