import React from 'react';
import './HeadSection.css';
import VirtuallHead from '../../assets/img/virtuall_01.png';
import VirtuAll from '../../assets/img/Group 41.png';

function HeadSection() {
  return (
    <div className="head-section">
      <div className="head-section-button_container">
        <a className="head-section-button" href='#lets-connect'>
          Let’s connect
        </a>
      </div>

      <div className="head-section-title_container">
        <img src={VirtuAll} className="head-section-title_logo"/>
        
        <div className="head-section-title_title">
          Transforming any industry through the creation of innovative products
          that connect the world in a sustainable and profitable way.
        </div>
      </div>


      <img src={VirtuallHead} className="head-section-img"/>
      
    </div>
  );
}

export default HeadSection;
