import React from 'react';
import './AboutSection.css';
import SustainabilityImage from '../../assets/img/virtuall_02.png';
import TransparencyImage from '../../assets/img/virtuall_03.png';

function AboutSection() {
  return (
    <div className="about-section">
      <div className="about-section-text-container">
        <span className="about-section-text-title">
          the virtue of innovation
        </span>
        <span>
          Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh 
          euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad 
          minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl 
          ut aliquip ex ea commodo consequat.
        </span>
      </div>

      <div className="about-section-container">
        <img src={SustainabilityImage} className="about-section-container-image"/>

        <div className="about-section-text-container sustainability">
          <span className="about-section-text-title">
            the virtue of sustainability
          </span>
          <span>
            Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy 
            nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi 
            enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis 
            nisl ut aliquip ex ea commodo consequat.
          </span>
        </div>
      </div>

      <div className="about-section-container transparency">
        <div className="about-section-text-container transparency">
          <span className="about-section-text-title">
            the virtue of transparency
          </span>
          <span>
            Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh 
            euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim 
            ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl 
            ut aliquip ex ea commodo consequat.
          </span>
          <a className="about-section-button" href='#lets-connect'>
            Let’s connect
          </a>
        </div>

        <img src={TransparencyImage} className="about-section-container-transparency_image"/>
      </div>
      
    </div>
  );
}

export default AboutSection;
