import React from 'react';
import './ConnectSection.css';

function ConnectSection() {

  const handleSubmit = (e) => {
    e.preventDefault();
    const formData = {
      name: e.target.name.value,
      email: e.target.email.value,
      subject: e.target.subject.value,
      textarea: e.target.textarea.value,
    };

    window.Email.send({
      Host : "smtp.elasticemail.com",
      Username : "explore.3mail@gmail.com",
      Password : "00FDFD2F22231767E53785886A2BF2134F31",
      To : `info@virtuall.company`,
      From : `explore.3mail@gmail.com`,
      Subject : `${formData.subject}`,
      Body : `Name: ${formData.name}\nEmail: ${formData.email}\nSubject: ${formData.subject}\nBody: ${formData.textarea}`
  })

  }

  return (
    <div className="connect-section" id='lets-connect'>
      <div className="connect-section-container">
          <span className="connect-section-container-title">
            Let’s connect.
          </span>

          <form onSubmit={handleSubmit} className="form">
            <div className="form-item-container">
              <label className="form-item-label">Name</label>
              <input type="text" name='name' required className="form-item-input"/>
            </div>

            <div className="form-item-container">
              <label className="form-item-label">E-mail</label>
              <input type="email" name='email' required className="form-item-input"/>
            </div>

            <div className="form-item-container">
              <label className="form-item-label">Subject</label>
              <select required className="form-item-input select" name='subject'>
                <option value="0">I’d like you to help me change my business</option>
                <option value="1">I want to work for virtuall</option>
                <option value="2">I want to organise a virtual / hybrid / physical event</option>
                <option value="3">I want to promote my space / place</option>
                <option value="3">I’d like to invest into virtuall and your products</option>
              </select>
            </div>

            <div className="form-item-container textarea">
              <label className="form-item-label textarea">How can we help you?</label>
              <textarea required className="form-item-input textarea" name='textarea'/>
            </div>

            <div className="form-button-group">
              <button type='submit' className="form-button-group_button">
                Send
              </button>
            </div>
          </form>
      </div>
    </div>
  );
}

export default ConnectSection;
