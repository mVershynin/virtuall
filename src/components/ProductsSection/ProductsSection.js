import React from 'react';
import './ProductsSection.css';
import Eventopedia from '../../assets/img/Group 40.png';
import GoVirtual from '../../assets/img/Group 9.png';
import Explore from '../../assets/img/Path 53.png';

function ProductsSection() {
  return (
    <div className="products-section">
      <div className="products-section-container">
        <span className="products-section-container_title">
          Our leading products are all about connections
        </span>

        <div className="products-section-container-product">
          <div className="products-section-container-product_blob">
            <img src={Explore} />
          </div>

          <div className="products-section-container-product_about">
            <span>
              Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy 
              nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi 
              enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis 
              nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in 
              hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu 
              feugiat nulla facilisis at vero eros et
            </span>

            <button className="products-section-container-product_button">
              visit the website
            </button>
          </div>
        </div>

        <div className="products-section-container-product govirtual">
          <div className="products-section-container-product_about govirtual">
            <span>
              Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy 
              nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi 
              enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis 
              nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in 
              hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu 
              feugiat nulla facilisis at vero eros et
            </span>

            <button className="products-section-container-product_button govirtual">
              visit the website
            </button>
          </div>

          <div className="products-section-container-product_blob govirtual">
            <img src={GoVirtual} />
          </div>
        </div>

        <div className="products-section-container-product">
          <div className="products-section-container-product_blob eventopedia">
            <img src={Eventopedia} />
          </div>

          <div className="products-section-container-product_about eventopedia">
            <span>
              Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy 
              nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi 
              enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis 
              nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in 
              hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu 
              feugiat nulla facilisis at vero eros et
            </span>

            <button className="products-section-container-product_button">
              visit the website
            </button>
          </div>
        </div>
      </div>
    </div>
  );
}

export default ProductsSection;
